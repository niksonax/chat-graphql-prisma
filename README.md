## Server

To start server:

```
cd server/
yarn run dev
```

## Client

To start client:

```
cd client/
yarn run start
```
