import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { POST_MESSAGE_MUTATION, MESSAGE_QUERY } from "../queries";
import "./MessageInput.css";

function MessageInput(props) {
  const [text, setText] = useState("");

  const _updateStoreAfterSendingMessage = (store, newMessage) => {
    const orderBy = "createdAt_ASC";
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
      },
    });
    data.messages.messageList.unshift(newMessage);
    store.writeQuery({
      query: MESSAGE_QUERY,
      data,
    });
  };

  return (
    <div className="message-input-container">
      <textarea
        className="message-input"
        onChange={(e) => setText(e.target.value)}
        value={text}
      />

      <Mutation
        mutation={POST_MESSAGE_MUTATION}
        variables={{ text }}
        update={(store, { data: { postMessage } }) => {
          _updateStoreAfterSendingMessage(store, postMessage);
        }}
      >
        {(postMutation) => (
          <button className="send-msg-btn" onClick={postMutation}>
            Send
          </button>
        )}
      </Mutation>
    </div>
  );
}

export default MessageInput;
