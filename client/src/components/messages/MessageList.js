import React, { useEffect } from "react";
import "./MessageList.css";
import { Query } from "react-apollo";
import { useQuery } from "@apollo/react-hooks";
import Message from "./Message";
import { MESSAGE_QUERY, NEW_MESSAGES_SUBSCRIPTION } from "../../queries";

function MessageList() {
  const orderBy = "createdAt_ASC";

  const _subscribeToNewMessages = (subscribeToMore) => {
    subscribeToMore({
      document: NEW_MESSAGES_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(
          ({ id }) => id === newMessage.id
        );
        if (exists) return prev;

        return {
          ...prev,
          messages: {
            messageList: [newMessage, ...prev.messages.messageList],
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename,
          },
        };
      },
    });
  };

  return (
    <Query query={MESSAGE_QUERY} variables={{ orderBy }}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading) return <div className="loader">Loading...</div>;
        if (error) return <div>Error</div>;
        _subscribeToNewMessages(subscribeToMore);

        const {
          messages: { messageList },
        } = data;
        console.log(messageList);

        return (
          <div className="messages-list">
            {messageList.map((message) => {
              return <Message key={message.id} {...message} />;
            })}
          </div>
        );
      }}
    </Query>
  );
}

export default MessageList;
