import React from "react";
import "./Message.css";
import ResponseList from "../responses/ResponseList";

function Message(props) {
  const { id, text, responses } = props;
  const displayId = `#${id.toString().slice(-5)}`;

  return (
    <div className="message">
      <div className="message-header">{displayId}</div>
      <div className="message-body">{text}</div>
      <div className="message-footer"></div>

      <ResponseList messageId={id} responses={responses} />
    </div>
  );
}

export default Message;
