import React from "react";
import "./Response.css";

function Response(props) {
  const { id, text } = props;
  const displayId = `#${id.toString().slice(-5)}`;

  return (
    <div className="response">
      <span className="response-header">{displayId}</span>
      <div className="response-body">{text}</div>
    </div>
  );
}

export default Response;
