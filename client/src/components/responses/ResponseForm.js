import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { MESSAGE_QUERY, POST_RESPONSE_MUTATION } from "../../queries";

function ResponseForm(props) {
  const { messageId, toggleForm } = props;
  const [text, setText] = useState("");

  const _updateStoreAfterAddingResponse = (store, newResponse, messageId) => {
    const orderBy = "createdAt_ASC";
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
      },
    });
    const responsedMessage = data.messages.messageList.find(
      (message) => message.id === messageId
    );
    responsedMessage.responses.push(newResponse);
    store.writeQuery({ query: MESSAGE_QUERY, data });
    toggleForm(false);
  };

  return (
    <div className="response-input-container">
      <textarea
        className="response-input"
        onChange={(e) => setText(e.target.value)}
        value={text}
      />
      <Mutation
        mutation={POST_RESPONSE_MUTATION}
        variables={{ messageId, text }}
        update={(store, { data: { postResponse } }) => {
          _updateStoreAfterAddingResponse(store, postResponse, messageId);
        }}
      >
        {(postMutation) => (
          <button className="response-input-btn" onClick={postMutation}>
            Send
          </button>
        )}
      </Mutation>
    </div>
  );
}

export default ResponseForm;
