import React, { useState } from "react";
import "./ResponseList.css";
import Response from "./Response";
import ResponseForm from "./ResponseForm";

function ResponseList(props) {
  const { messageId, responses } = props;
  const [responseForm, setResponseForm] = useState(false);

  return (
    <div className="responses-list">
      <button
        className="response-btn"
        onClick={() => {
          setResponseForm(!responseForm);
        }}
      >
        {responseForm ? "X" : "Reply"}
      </button>
      {responses.map((response) => {
        return <Response key={response.id} {...response} />;
      })}
      {responseForm ? (
        <ResponseForm messageId={messageId} toggleForm={setResponseForm} />
      ) : null}
    </div>
  );
}

export default ResponseList;
