import "./App.css";
import MessageList from "./components/messages/MessageList";
import MessageInput from "./components/MessageInput";

function App() {
  return (
    <div className="App">
      <MessageList />
      <MessageInput />
    </div>
  );
}

export default App;
