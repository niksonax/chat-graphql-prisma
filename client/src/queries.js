import gql from "graphql-tag";

export const MESSAGE_QUERY = gql`
  query messageQuery($orderBy: MessageOrderByInput) {
    messages(orderBy: $orderBy) {
      count
      messageList {
        id
        text
        responses {
          id
          text
        }
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($text: String!) {
    postMessage(text: $text) {
      id
      text
      responses {
        id
        text
      }
    }
  }
`;

export const POST_RESPONSE_MUTATION = gql`
  mutation PostMutation($messageId: ID!, $text: String!) {
    postResponse(messageId: $messageId, text: $text) {
      id
      text
    }
  }
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      responses {
        id
        text
      }
    }
  }
`;
